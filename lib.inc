%define    exit_id 60
%define    print_id 1
%define    read_id 0
%define    stdout_id 1
%define    stdin_id 0
%define    byte_num 1
%define    newline_sym 10
%define    err_code 0
%define    ws_sym 0x20
%define    tab_sym 0x9
%define    end_sym 0xA

section .text
 ; Принимает код возврата и завершает текущий процесс
exit: 
    mov  rax, exit_id     
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:            
    xor rax, rax       
    .count:                
    cmp byte [rdi+rax], 0  
    je .end                      
    inc rax                      
    jmp .count                   
    .end:                        
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:            
    push rdi   
    call string_length     
    pop rdi

    mov  rdx, rax      
    mov  rsi, rdi      

    mov  rax, print_id                     
    mov  rdi, stdout_id            
    syscall                                                                                                                
    ret

; Принимает код символа и выводит его в stdout
print_char:
    
    push rdi
    mov rax, print_id ; 'write' syscall identifier
    mov rsi, rsp ; where do we take data from
    mov rdi, stdout_id ; stdout file descriptor
    mov rdx, byte_num ; the amount of bytes to write               
    syscall  
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
    mov rdi, newline_sym

    call print_char

    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.


print_uint:
    mov rax, rdi
    push 0
    .loop: 
        xor rsi, rsi
        xor rdx, rdx 
        mov rsi, 10
        div rsi
        add rdx, '0'
        push rdx
        test rax, rax
        jz .out
        jmp .loop
    .out:

    .loop2:
        pop rdi
        cmp rdi, 0
            jz .out2
        push rdi
        call print_char
        pop rdi
        jmp .loop2
    .out2:
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    js .negout
        call print_uint
    jmp .out
    .negout: 
        push rdi
        mov rdi, '-'
        push rdi
        call print_char
        pop rdi
        pop rdi
        neg rdi
        call print_uint
    .out:
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 0
    .loop:
        movsx r8, byte[rdi+rax]
        movsx r9, byte[rsi+rax]
        cmp r8, 0
            je .outIf
        
        cmp r9, 0
            je .unequal

        cmp r8, r9
        jne .unequal
        
        inc rax
        jmp .loop

        .outIf:
        cmp r9, 0
        jne .unequal
    .out:

    mov rax, 1
    ret
    .unequal:
        mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0

    mov rax, read_id
    mov rsi, rsp 
    mov rdi, stdin_id 
    mov rdx, byte_num
    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word: ;rdi: адрес rsi: размер
    mov rdx, 0 ; длинна прочитанного слова
    .loop1: ; пропускаем пробельные символы
        push rsi
        push rdi
        push rdx
        call read_char
        pop rdx
        pop rdi
        pop rsi

        cmp rax, err_code
        je .badOut
               
        cmp rax, ws_sym
        je .loop1
         
        cmp rax, tab_sym
        je .loop1 

        cmp rax, end_sym
        je .loop1
    jmp .first_char

    
    .loop2: ; считываем слово
        push rsi
        push rdi
        push rdx
        call read_char
        pop rdx
        pop rdi
        pop rsi
        .first_char:

        cmp rax, err_code
        je .out

        cmp rax, ws_sym
        je .out
         
        cmp rax, tab_sym
        je .out 

        cmp rax, end_sym
        je .out

        cmp rsi - 1, rdx
        je .badOut

        mov [rdi + rdx], rax
        inc rdx
        jmp .loop2


    .out:
    mov byte [rdi + rdx + 1], 0
    mov rax, rdi
    ret
    .badOut:
    mov rax, 0
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: ;rdi : указатель на строку

    .loop1: ; пропускаем нечисловые символы
        cmp byte[rdi], '0'
        jae .if
        jmp .ifOut
    
        .if:
            cmp byte[rdi], '9'
            jbe .out1
        .ifOut:
        cmp byte [rdi], 0
        je .badOut
        inc rdi
        jmp .loop1
    .out1:

    xor rax, rax
    xor rdx, rdx

    .loop2: ; считываем число
        cmp byte [rdi], end_sym
        je .out

        cmp byte [rdi], err_code
        je .out
         
        cmp byte [rdi], end_sym
        je .out 

        cmp byte [rdi], tab_sym
        je .out

        cmp byte [rdi], '0'
        jb .out
        cmp byte [rdi], '9'
        ja .out

        imul rax, 10
        push rsi
        movsx rsi, byte [rdi]
        add rax, rsi
        sub rax, '0'
        inc rdi
        inc rdx
        pop rsi
    jmp .loop2

    .out:
    ret

    .badOut:
    mov rdx, 0  
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    .loop1: ; пропускаем нечисловые символы
        cmp byte[rdi], '-'
        je .neg

        cmp byte[rdi], '0'
        jae .if
        jmp .ifOut
    
        .if:
            cmp byte[rdi], '9'
            jbe .pos
            
        .ifOut:
        inc rdi
    jmp .loop1
        
    .neg:
    inc rdi
    call parse_uint
    neg rax

    cmp rdx, 0
    je .badOut

    inc rdx
    jmp .out
    
    .pos:
    call parse_uint
    jmp .out

    .badOut:
    mov rdx, 0

    .out:

    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ;rdi: указатель на строку, ; rsi: буффер ; rdx: длинна
    xor rax, rax
.loop:
    cmp rax, rdx
    je .badOut

    mov rcx, [rdi + rax]

    mov [rsi + rax], rcx

    cmp rcx, 0
    je .out
    inc rax
    jmp .loop
.out:
dec rax
ret

.badOut:
mov rax, 0
ret
